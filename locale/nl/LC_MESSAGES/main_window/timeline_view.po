# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-17 00:50+0000\n"
"PO-Revision-Date: 2023-03-17 09:47+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../main_window/timeline_view.rst:1
msgid "digiKam Main Window Timeline View"
msgstr "Tijdlijnweergave in het hoofdvenster van digiKam"

#: ../../main_window/timeline_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, timeline, days, weeks, months, years"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"eren, gemakkelijk, tijdlijn, dagen, weken, maanden, jaren"

#: ../../main_window/timeline_view.rst:14
msgid "Time-Line View"
msgstr "Tijdlijnweergave"

#: ../../main_window/timeline_view.rst:20
msgid "The Timeline View"
msgstr "De tijdlijnweergave"

#: ../../main_window/timeline_view.rst:22
msgid ""
"The Timeline View shows a timescale-adjustable histogram of the numbers of "
"images per **Time Unit** which is selectable by a drop down field. Available "
"graininess of time are **Day**, **Week**, **Month**, and **Year**."
msgstr ""
"De tijdlijnweergave toont een aanpasbaar histogram van de tijdschaal van het "
"aantal afbeeldingen per **Tijdseenheid** die met een afrolveld is te "
"selecteren. Beschikbare granulariteiten van de tijd zijn **Dag**, **Week**, "
"**Maand** en **Jaar**."

#: ../../main_window/timeline_view.rst:24
msgid ""
"To the right of that you can choose between a **Linear** or **Logarithmic** "
"histogram using scaling buttons. A selection frame can be moved over the "
"histogram and to display the photographs out of a certain time frame, just "
"click on the corresponding histogram bar. You are not restricted to one bar: "
"with :kbd:`Shift+left` click you can select a range of bars from the "
"histogram, and with :kbd:`Ctrl+left` click you can select more single bars "
"to the first one."
msgstr ""
"Rechts daarvan kunt u kiezen tussen een **Lineair** of **Logaritmisch** "
"histogram met een schaalknop. Een selectieframe kan over het histogram "
"bewegen om foto's uit een bepaald tijdframe te tonen, klik eenvoudig op de "
"bijbehorende histogrambalk. U bent niet beperkt tot één balk: met klik :kbd:"
"`Shift+Links` kunt een reeks balken selecteren uit het histogram en met :kbd:"
"`Ctrl+Links` kunt u meer enkele balken naast de eerste selecteren."

#: ../../main_window/timeline_view.rst:31
msgid "Timeline Selection Screencast"
msgstr "Schermfilmpje van tijdlijnselectie"

#: ../../main_window/timeline_view.rst:33
msgid ""
"In the field right below you can enter a title and save your selection. It "
"will then appear in the **Searches** list field below. But the best is still "
"to come: the Timeline View offers a search for a search. If you have a lot "
"more searches saved in the database, the adaptive search field at the bottom "
"may help to find a certain entry in the list."
msgstr ""
"In het veld rechtsonder kunt u een titel invoeren en uw selectie opslaan. "
"Het zal dan verschijnen in het onderstaande veld met een lijst "
"**Zoekopdrachten**. Maar het beste moet nog komen: de Tijdlijnweergave biedt "
"een zoekopdracht voor een zoekopdracht! Als u heel wat meer zoekopdrachten "
"hebt opgeslagen in de database zal het aanpasbare zoekopdrachtveld onderaan "
"kunnen helpen bij het vinden van een bepaald item in de lijst."

#: ../../main_window/timeline_view.rst:35
msgid ""
"The date-range of the histogram is populated with the time stamp of items "
"registered in the database. For each date matching the histogram bars time-"
"resolution, an item is counted in the statistics. Long bars correspond to "
"dates where a lots of items have been taken in the same graininess-range."
msgstr ""
"De datumreeks van het histogram is bevolkt met de tijdstempels van items "
"geregistreerd in de database. Voor elke datum overeenkomend met de "
"tijdresolutie van de histogrambalken, wordt en item geteld in de "
"statistieken. Lange balken komen overeen met datums waar veel items zich "
"bevinden in dezelfde granulariteitsreeks."

#: ../../main_window/timeline_view.rst:37
msgid ""
"The icon-view gives the search results of the selection of date. You can "
"select wanted items to post-process contents on batch queue manager or "
"export items to the Internet. From Right sidebar you can filter icon view "
"contents by database properties."
msgstr ""
"De pictogramweergave geeft de zoekresultaten van de selectie van datums. U "
"kunt gewenste items selecteren om inhoud na te bewerken in de "
"takenwachtrijbeheerder of ze exporteren naar het internet. Vanuit de rechter "
"zijbalk kunt u inhoud van de pictogramweergave filteren op eigenschappen in "
"de database."
