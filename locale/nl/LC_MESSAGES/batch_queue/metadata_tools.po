# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-25 00:50+0000\n"
"PO-Revision-Date: 2023-01-25 11:06+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: ../../batch_queue/metadata_tools.rst:1
msgid "digiKam Batch Queue Manager Metadata Tools"
msgstr "Takenwachtrijbeheerder van digiKam hulpmiddelen voor metagegevens"

#: ../../batch_queue/metadata_tools.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, batch, metadata, pick, labels, rating, color, comment, caption, "
"title"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, bulk, metagegevens, kiezen, labels, waardering, kleur, "
"commentaar, opschrift, titel"

#: ../../batch_queue/metadata_tools.rst:14
msgid "Metadata Tools"
msgstr "Hulpmiddelen voor metagegevens"

#: ../../batch_queue/metadata_tools.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../batch_queue/metadata_tools.rst:18
msgid ""
"The Batch Manager metadata tools are used to apply the :ref:`Digital Asset "
"Management <asset_management>` by managing metadata values in items hosted "
"in a Queue, to help to classify contents in your collection or to prepare "
"for sharing outside the box."
msgstr ""
"De hulpmiddelen van metagegevens takenbeheerder worden gebruikt om het :ref:"
"`Digitale Assetbeheer <asset_management>` toe te passen door "
"metagegevenswaarden in items opgeslagen in een wachtrij te beheren, om te "
"helpen bij het classificeren van inhoud in uw verzameling of om voor te "
"bereiden voor delen buiten het systeem."

#: ../../batch_queue/metadata_tools.rst:23
msgid "Image Quality Sort"
msgstr "Sorteren op kwaliteit"

#: ../../batch_queue/metadata_tools.rst:25
msgid ""
"This tool parse your items to assign a **Pick Label** automatically "
"depending of the aesthetic value of the contents. You can use the full "
"automated solution based on deep-learning engine (recommended), or use a "
"manual settings for each criteria. These settings are fully explained in "
"the :ref:`Setup Quality <imgqsort_settings>` section from this manual."
msgstr ""
"Dit hulpmiddel ontleed uw items om een **Keuzelabel** automatisch toe te "
"kennen afhankelijk van de esthetische waarde van de inhoud. U kunt de "
"volledig geautomatiseerde oplossing gebaseerd op de deep-learning engine "
"(aanbevolen) gebruiken of handmatige instellingen voor elk criterium. Deze "
"instellingen zijn geheel uitgelegd in de sectie :ref:`Kwaliteit opzetten "
"<imgqsort_settings>` uit deze handleiding."

#: ../../batch_queue/metadata_tools.rst:31
msgid "The Batch Queue Manager Tool to Sort Items by Image Quality"
msgstr ""
"Het hulpmiddel items sorteren op kwaliteit van de afbeelding in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/metadata_tools.rst:36
msgid "Apply Metadata Template"
msgstr "Pas het metagegevenssjabloon toe"

#: ../../batch_queue/metadata_tools.rst:38
msgid ""
"This tool is dedicated to apply a metadata template to your items in order "
"to set copyright and description information about the contents. The "
"**Template** drop-down field lets you choose one of your metadata templates "
"you customized in :ref:`Templates Settings <templates_settings>`. You can "
"access the **Template Manager** also with the edit button to the right of "
"the drop-down field."
msgstr ""
"Dit hulpmiddel is speciaal voor het toepassen van een metagegevenssjabloon "
"op uw items om informatie over copyright en beschrijving over de inhoud in "
"te stellen. Het afrolveld **Sjabloon** laat u een van uw "
"metagegevenssjablonen kiezen die u hebt aangepast met :ref:"
"`Sjablooninstellingen <templates_settings>`. U krijgt ook toegang tot de "
"**Sjabloonbeheerder** met de knop bewerken rechts van het afrolveld."

#: ../../batch_queue/metadata_tools.rst:44
msgid "The Batch Queue Manager Tool to Apply Metadata Template"
msgstr ""
"Het hulpmiddel metagegevenssjabloon toepassen in de takenwachtrijbeheerder"

#: ../../batch_queue/metadata_tools.rst:49
msgid "Translate Metadata"
msgstr "Metagegevens vertalen"

#: ../../batch_queue/metadata_tools.rst:51
msgid ""
"This tool allows to translate automatically some alternative language string "
"from metadata using an online translator service. Translatable entries are:"
msgstr ""
"Dit hulpmiddel biedt het automatisch vertalen enige alternatieve taal "
"tekenreeksen uit metagegevens met een online vertaalservice. Vertaalbare "
"items zijn:"

#: ../../batch_queue/metadata_tools.rst:53
msgid ":ref:`Title <captions_comments>`."
msgstr ":ref:`Titel <captions_comments>`."

#: ../../batch_queue/metadata_tools.rst:54
msgid ":ref:`Captions <captions_comments>`."
msgstr ":ref:`Opschriften <captions_comments>`."

#: ../../batch_queue/metadata_tools.rst:55
msgid ":ref:`Copyrights <authorship_copyright>`."
msgstr ":ref:`Copyrights <authorship_copyright>`."

#: ../../batch_queue/metadata_tools.rst:56
msgid ":ref:`Usage Terms <authorship_copyright>`."
msgstr ":ref:`Gebruiksbepalingen <authorship_copyright>`."

#: ../../batch_queue/metadata_tools.rst:58
msgid ""
"The translations settings can be tuned with the dedicated :ref:`Localize "
"Setup Panel <localize_settings>`. A list of languages to append or fix in "
"selected alternative language strings. To add a new language in this list, "
"use the drop-down button on the left of **Translate to** option. To remove a "
"language, use the context menu over this list."
msgstr ""
"De instellingen voor vertalen kan afgeregeld worden met het speciale :ref:"
"`Instellingspaneel lokaliseren <localize_settings>`. Een lijst met toe te "
"voegen talen of repareren in geselecteerde alternatieve taaltekenreeksen. Om "
"een nieuwe taal aan dezes lijst toe te voegen, gebruik de afrolknop links "
"van de optie  **Vertalen naar**. Om een taal te verwijderen, gebruik het "
"contextmenu op deze lijst."

#: ../../batch_queue/metadata_tools.rst:64
msgid "The Batch Queue Manager Tool to Translate Strings in Metadata"
msgstr ""
"Het hulpmiddel om tekenreeksen te vertalen in metagegevens in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/metadata_tools.rst:69
msgid "Assign Captions"
msgstr "Opschriften toekennen"

#: ../../batch_queue/metadata_tools.rst:71
msgid ""
"This tool allows to assign **Title**, **Captions**, and **Author** "
"properties to items from a Queue. For details about these properties, take a "
"look to the :ref:`Captions section  <captions_view>` from this manual."
msgstr ""
"Dit hulpmiddel biedt het toekennen van eigenschappen als **Titel**, "
"**Opschriften** en **Auteur** aan items in een wachtrij. Voor details over "
"deze eigenschappen, neem een kijkje in :ref:`sectie Opschriften "
"<captions_view>` van deze handleiding."

#: ../../batch_queue/metadata_tools.rst:77
msgid "The Batch Queue Manager Tool to Assign Captions to Items"
msgstr ""
"Het hulpmiddel opschriften toekennen aan items in de takenwachtrijbeheerder"

#: ../../batch_queue/metadata_tools.rst:82
msgid "Assign Labels"
msgstr "Labels toewijzen"

#: ../../batch_queue/metadata_tools.rst:84
msgid ""
"This tool allows to assign **Pick Label**, **Rating**, and **Color Label** "
"to items from a Queue. In oppposite to **Quality Sort** tool which apply a "
"Pick Label automatically, this tool just tune the Pick Label value manually. "
"For details about these properties, take a look to the :ref:`Captions "
"section <captions_view>` from this manual."
msgstr ""
"Dit hulpmiddel biedt het toekennen van een **Keuzelabel**, **Waardering** en "
"**Kleurlabel** aan items in een wachtrij. In tegenstelling tot het "
"hulpmiddel **Sorteren op kwaliteit** die een keuzelabel automatisch toekent, "
"doet dit hulpmiddel het afregelen van de waarde van het keuzelabel "
"handmatig. Voor details over deze eigenschappen, kijk in de :ref:`sectie "
"Opschriften <captions_view>` uit deze handleiding."

#: ../../batch_queue/metadata_tools.rst:90
msgid "The Batch Queue Manager Tool to Assign Labels to Items"
msgstr "Het hulpmiddel labels toekennen aan items in de takenwachtrijbeheerder"

#: ../../batch_queue/metadata_tools.rst:95
msgid "Remove Metadata"
msgstr "Metagegevens verwijderen"

#: ../../batch_queue/metadata_tools.rst:97
msgid ""
"This tool to drop parts of metadata from files, into the **Exif**, **IPTC**, "
"and **XMP** chunks. More precisely you can select from the delegate drop-"
"down menus the following sections:"
msgstr ""
"Dit hulpmiddel is er om delen van metagegevens te verwijderen uit bestanden, "
"in de **Exif**, **IPTC** en **XMP** gedeelten. Meer precies u kunt uit de "
"speciale afrolmenu's de volgende secties selecteren:"

#: ../../batch_queue/metadata_tools.rst:100
msgid "**Completely**: drop all Exif entries."
msgstr "**Volledig**: verwijder alle Exif-items."

#: ../../batch_queue/metadata_tools.rst:101
#: ../../batch_queue/metadata_tools.rst:108
#: ../../batch_queue/metadata_tools.rst:113
msgid "**Date**: drop the time-stamp."
msgstr "**Datum**: verwijder het tijdstempel."

#: ../../batch_queue/metadata_tools.rst:102
msgid "**GPS**: drop geolocation information."
msgstr "**GPS**: verwijder de informatie over geo-locatie."

#: ../../batch_queue/metadata_tools.rst:103
msgid "**XPKeywords**: drop the Windows keywords"
msgstr "**XPKeywords**: verwijder de Windows sleutelwoorden"

#: ../../batch_queue/metadata_tools.rst:104
msgid "Exif:"
msgstr "Exif:"

#: ../../batch_queue/metadata_tools.rst:104
msgid "**Comments and Descriptions**: all captions entries."
msgstr "**Commentaar en beschrijvingen**: alle opschriftitems."

#: ../../batch_queue/metadata_tools.rst:107
msgid "**Completely**: drop all IPTC entries."
msgstr "**Volledig**: verwijdert alle IPTC-items."

#: ../../batch_queue/metadata_tools.rst:109
msgid "IPTC:"
msgstr "IPTC:"

#: ../../batch_queue/metadata_tools.rst:109
msgid "**Caption**: drop the content descriptions."
msgstr "**Opschrift**: verwijdert de inhoud van beschrijvingen."

#: ../../batch_queue/metadata_tools.rst:112
msgid "**Completely**: drop all XMP entries."
msgstr "**Volledig**: verwijdert alle XMP-items."

#: ../../batch_queue/metadata_tools.rst:114
msgid "**DigiKam**: drop all information from application namespace."
msgstr ""
"**DigiKam**: verwijder alle informatie uit de naamruimte van de toepassing."

#: ../../batch_queue/metadata_tools.rst:115
msgid ""
"**DigiKam image history**: drop only the versioning data from the "
"application namespace."
msgstr ""
"**Afbeeldingsgeschiedenis van digiKam**: verwijder alleen de gegevens over "
"met versies uit de naamruimte van de toepassing."

#: ../../batch_queue/metadata_tools.rst:116
msgid "**Dublin Core**: drop only the entries from Dublin Core namespace."
msgstr ""
"**Dublin Core**: verwijdert alleen de items uit de naamruimte Dublin Core."

#: ../../batch_queue/metadata_tools.rst:117
msgid ""
"**Exif**: drop only the translated Exif information stored in the dedicated "
"namespace."
msgstr ""
"**Exif**: verwijder alleen de vertaalde Exif-informatie opgeslagen in de "
"toegewezen naamruimte."

#: ../../batch_queue/metadata_tools.rst:118
msgid "**Video**: drop only the entries from the Video namespace."
msgstr "**Video**: verwijdert alleen de items uit de naamruimte Video."

#: ../../batch_queue/metadata_tools.rst:119
msgid "XMP:"
msgstr "XMP:"

#: ../../batch_queue/metadata_tools.rst:119
msgid ""
"**Caption, comment and description**: drop all entries described the "
"contents."
msgstr ""
"**Opschrift, commentaar en beschrijving**: verwijder alle items die de "
"inhoud beschrijven."

#: ../../batch_queue/metadata_tools.rst:125
msgid "The Batch Queue Manager Tool to Remove Metadata from Items"
msgstr ""
"Het hulpmiddel voor verwijderen van metagegevens uit items in de "
"takenwachtrijbeheerder"

#: ../../batch_queue/metadata_tools.rst:130
msgid "Time Adjust"
msgstr "Tijdbijstelling"

#: ../../batch_queue/metadata_tools.rst:132
msgid ""
"This tool allows to adjust the time-stamp entries in file metadata. It use "
"the same settings than :ref:`stand alone version <time_adjust>` of this "
"plugin available from **Main Window** from :menuselection:`Item --> Adjust "
"Date & Time` menu entry."
msgstr ""
"Dit hulpmiddel biedt het bijstellen van het tijdstempel in metagegevens van "
"het bestand. Het gebruikt dezelfde instellingen als :ref:`alleen staande "
"versie <time_adjust>` van deze plug-in beschikbaar in het **Hoofdvenster** "
"uit het menu-item :menuselection:`Item --> Datum & tijd aanpassen`."

#: ../../batch_queue/metadata_tools.rst:138
msgid "The Batch Queue Manager Tool to Adjust Items Time-Stamp"
msgstr ""
"Het hulpmiddel voor het aanpassen van het tijdstempel van items in de "
"takenwachtrijbeheerder"

#~ msgid "Assign Template"
#~ msgstr "Sjabloon toekennen"
