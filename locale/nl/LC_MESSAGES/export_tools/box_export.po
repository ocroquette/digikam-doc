# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-21 00:52+0000\n"
"PO-Revision-Date: 2023-02-21 16:41+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../export_tools/box_export.rst:1
msgid "digiKam Export to Box Web-Service"
msgstr "Naar Box-webservice exporteren van digiKam"

#: ../../export_tools/box_export.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, box, export"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, box, exporteren"

#: ../../export_tools/box_export.rst:14
msgid "Export To Box"
msgstr "Exporteren naar Box"

#: ../../export_tools/box_export.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../export_tools/box_export.rst:18
msgid "This tool allows the user to upload photos to the Box web-service."
msgstr ""
"Het hulpmiddel stelt de gebruiker in staat foto's naar de Box-webservice te "
"uploaden."

#: ../../export_tools/box_export.rst:20
msgid ""
"`Box <https://en.wikipedia.org/wiki/Box_(company)>`_ is a cloud-based "
"content management, collaboration, and file sharing tools. The application "
"allows users to store and manage files in an online folder system accessible "
"from any device. Users can then comment on the files, share them, apply "
"workflows, and implement security and governance policies."
msgstr ""
"`Box <https://en.wikipedia.org/wiki/Box_(company)>`_ is een op cloud "
"gebaseerd beheer van inhoud, samenwerking en hulpmiddelen voor delen van "
"bestanden. De toepassing biedt gebruikers het opslaan van beheren van "
"bestanden in een online mapsysteem toegankelijk vanaf elk apparaat. "
"Gebruikers kunnen dan commentaar geven op de bestanden, ze delen, "
"werkmethoden toepassen en beleid voor veiligheid en beheer implementeren."

#: ../../export_tools/box_export.rst:22
msgid ""
"The tool can be used to upload a selection of images from your collections "
"to the remote Box server using the Internet."
msgstr ""
"Het hulpmiddel kan gebruikt worden om een selectie van afbeeldingen uit uw "
"verzamelingen te uploaden naar de Box-server op afstand via het internet."

#: ../../export_tools/box_export.rst:24
msgid ""
"When accessing the tool for the first time by the :menuselection:`Export --> "
"Export to Box` :kbd:`Ctrl+Alt+Shift+B` menu entry, you are taken through the "
"process of obtaining a token which is used for authentication purposes. The "
"following dialog will popup and a browser window will be launched you will "
"log in to Box:"
msgstr ""
"Bij de eerste keer gebruiken van het hulpmiddel via het menu-item :"
"menuselection:`Exporteren --> Naar Box exporteren` :kbd:`Ctrl+Alt+Shift+B`, "
"wordt u door het proces van het verkrijgen van een token geleid, wat wordt "
"gebruikt voor authenticatie. De volgende dialoog zal verschijnen en een "
"browservenster zal gestart worden om u aan te melden bij Box:"

#: ../../export_tools/box_export.rst:30
msgid "The Box Login Dialog"
msgstr "De aanmelddialoog voor Box"

#: ../../export_tools/box_export.rst:32
msgid ""
"After successful sign-up digiKam will be allowed to send photos to the Box "
"website. You will be presented with the following page:"
msgstr ""
"Na met succes aangemeld te zijn zal digiKam worden toegestaan om foto's te "
"verzenden naar de Box-website. U krijgt de volgende pagina te zien:"

#: ../../export_tools/box_export.rst:39
msgid "The Box Authorize Dialog"
msgstr "De autorisatiedialoog voor Box"

#: ../../export_tools/box_export.rst:41
msgid ""
"Then, simply authorize application and close the web browser. Return to the "
"host application dialog, you will see the interface used to upload photos to "
"Box."
msgstr ""
"Autoriseer daarna eenvoudig de toepassing en sluit de webbrowser. Keer terug "
"naar de dialoog van de toepassing, u zult het interface zien dat wordt "
"gebruikt om foto's te uploaden naar Box."

#: ../../export_tools/box_export.rst:47
msgid "The Box Export Tool Dialog"
msgstr "De hulpmiddeldialoog voor exporteren naar Box"

#: ../../export_tools/box_export.rst:49
msgid ""
"By default, the tool proposes to export the currently selected items from "
"the icon-view. The **+** Photos button can be used to append more items on "
"the list."
msgstr ""
"Standaard zal het hulpmiddel voorstellen om de nu geselecteerde items uit de "
"pictogramweergave te exporteren. De knop **+** foto's kan gebruikt worden om "
"meer items aan de lijst toe te voegen."

#: ../../export_tools/box_export.rst:51
msgid ""
"With the **Album** options, you can select the online folder to store files "
"to upload. You can **Create** new one and **Reload** the list on the combo-"
"box if online contents have been changed in Box web interface."
msgstr ""
"Met de opties van **Album** kunt u de online map selecteren om te uploaden "
"bestanden op te slaan. U kunt een nieuwe **Aanmaken** en de lijst "
"**Herladen** in het keuzevak als inhoud online is gewijzigd in het "
"webinterface van Box."

#: ../../export_tools/box_export.rst:53
msgid ""
"If the **Resize photos before uploading** option is selected, the photos "
"will be resized before transferring to Box. The values will be read from the "
"**JPEG quality** and **Maximum Dimension** settings, which can be used to "
"adjust the maximum height and the compression. The width calculation will be "
"done so as to have the aspect ratio conserved."
msgstr ""
"Als de optie **Foto's voor uploaden van grootte wijzigen** is geselecteerd, "
"zullen de foto's van grootte worden gewijzigd voor overbrengen naar Box. De "
"waarden zullen gelezen worden uit de instellingen **JPEG-kwaliteit** en "
"**Maximale afmetingen**, die gebruikt kan worden om de maximale hoogte en de "
"compressie aan te passen. De breedte berekening zal gedaan worden zodat de "
"beeldverhouding bewaard blijft."

#: ../../export_tools/box_export.rst:59
msgid "The Box Export Uploading in Progress"
msgstr "De voortgang van het uploaden tijdens exporteren naar Box"

#: ../../export_tools/box_export.rst:61
msgid ""
"Press **Start Upload** button to transfer items. You can click on the "
"**Close** button to abort the uploading of photos."
msgstr ""
"Druk op de knop **Uploaden starten** om items over te sturen. U kunt op de "
"knop **Sluiten** klikken om het uploaden van foto's af te breken."

#: ../../export_tools/box_export.rst:63
msgid "Finally, you can view the uploaded photos by visiting the Box website."
msgstr ""
"Tenslotte kunt u de geüploade foto's bekijken door de website van Box te "
"bezoeken."

#: ../../export_tools/box_export.rst:69
msgid "The Box Online Account Displaying the Uploaded Contents"
msgstr "Het online-account van Box die de geüploade inhoud toont"
